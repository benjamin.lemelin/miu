use std::io::{Stdout, stdout};

use crossterm::event::{Event as CrosstermEvent, EventStream, KeyCode as CrosstermKeyCode, KeyEventKind, KeyModifiers};
use eyre::Result;
use ratatui::Frame;
use ratatui::layout::Rect;
use tokio::sync::mpsc::{self, UnboundedSender as Sender};

type Terminal = ratatui::Terminal<Backend>;
type Backend = ratatui::backend::CrosstermBackend<Stdout>;

pub mod color;

/// Terminal UI.
#[derive(Debug)]
pub struct Tui;

impl Tui {
    /// Initialize terminal UI hooks and dependencies.
    ///
    /// Should only be called once, usually at the start of the program.
    pub fn init() {
        Self::setup_hooks()
    }

    /// Run terminal UI using provided component.
    pub fn run(root: impl Component<Event=ComponentEvent>) -> Result<()> {
        Self::setup_terminal()?;

        async fn run(mut root: impl Component<Event=ComponentEvent>) -> Result<()> {
            let mut terminal = Terminal::new(Backend::new(stdout()))?;
            let (tx, mut rx) = mpsc::unbounded_channel();
            tokio::spawn(Tui::event_loop(tx));

            loop {
                // Draw application,
                terminal.draw(|frame| root.draw(frame, frame.size()))?;

                // Poll next event.
                if let Some(event) = rx.recv().await {
                    if let TerminalEvent::Exit = event {
                        break;
                    } else if let Some(ComponentEvent::Exit) = root.update(&event) {
                        break;
                    }
                } else {
                    break;
                }
            }

            Ok(())
        }

        tokio::runtime::Builder::new_multi_thread()
            .enable_all()
            .build()
            .expect("tokio runtime should be able to run")
            .block_on(run(root))?;

        Self::teardown_terminal()
    }

    fn setup_terminal() -> Result<()> {
        use crossterm::{ExecutableCommand, terminal};

        let mut stdout = stdout();
        terminal::enable_raw_mode()?;
        stdout.execute(terminal::EnterAlternateScreen)?;
        Ok(())
    }

    fn teardown_terminal() -> Result<()> {
        use crossterm::{ExecutableCommand, terminal};

        let mut stdout = stdout();
        terminal::disable_raw_mode()?;
        stdout.execute(terminal::LeaveAlternateScreen)?;
        Ok(())
    }

    fn setup_hooks() {
        let panic = std::panic::take_hook();

        std::panic::set_hook(Box::new(move |e| {
            let _ = Self::teardown_terminal();
            panic(e)
        }));
    }

    async fn event_loop(tx: Sender<TerminalEvent>) -> Result<()> {
        use futures::StreamExt;

        let mut events = EventStream::new();

        while let Some(event) = events.next().await {
            match event {
                Ok(event) => {
                    if let Ok(event) = event.try_into() {
                        tx.send(event)?;
                    }
                }
                Err(_) => {
                    // Channel closed. Exit.
                    break;
                }
            }
        }

        Ok(())
    }
}

/// Terminal UI event.
#[derive(Debug)]
pub enum TerminalEvent {
    KeyPress(KeyCode),
    Resize { width: u16, height: u16 },
    Exit,
}

impl TryFrom<CrosstermEvent> for TerminalEvent {
    type Error = ();

    fn try_from(value: CrosstermEvent) -> Result<Self, Self::Error> {
        match value {
            CrosstermEvent::Key(event) if event.kind == KeyEventKind::Press => {
                if event.modifiers == KeyModifiers::CONTROL && event.code == CrosstermKeyCode::Char('c') {
                    Ok(Self::Exit)
                } else {
                    Ok(Self::KeyPress(event.code.try_into()?))
                }
            }
            CrosstermEvent::Resize(width, height) => {
                Ok(Self::Resize { width, height })
            }
            _ => {
                Err(())
            }
        }
    }
}

/// Keycode.
#[derive(Debug)]
pub enum KeyCode {
    Char(char),
    Esc,
    Up,
    Down,
    Left,
    Right,
    Enter,
    Backspace,
    Delete,
}

impl TryFrom<CrosstermKeyCode> for KeyCode {
    type Error = ();

    fn try_from(value: CrosstermKeyCode) -> Result<Self, Self::Error> {
        match value {
            CrosstermKeyCode::Char(char) => Ok(Self::Char(char)),
            CrosstermKeyCode::Esc => Ok(Self::Esc),
            CrosstermKeyCode::Up => Ok(Self::Up),
            CrosstermKeyCode::Down => Ok(Self::Down),
            CrosstermKeyCode::Left => Ok(Self::Left),
            CrosstermKeyCode::Right => Ok(Self::Right),
            CrosstermKeyCode::Enter => Ok(Self::Enter),
            CrosstermKeyCode::Backspace => Ok(Self::Backspace),
            CrosstermKeyCode::Delete => Ok(Self::Delete),
            _ => Err(())
        }
    }
}

/// Basic component event.
#[derive(Debug)]
pub enum ComponentEvent {
    Exit
}

/// Terminal UI component.
pub trait Component {
    /// Event produced by this component.
    type Event;

    /// Update the component.
    ///
    /// The component should send the event to his children. It may also return an event of its own
    /// to his parent.
    #[allow(unused_variables)]
    fn update(&mut self, event: &TerminalEvent) -> Option<Self::Event> {
        None
    }

    /// Draw the component on the frame buffer.
    ///
    /// The component should render on the provided frame buffer inside the specified area. It
    /// should also render his children.
    #[allow(unused_variables)]
    fn draw(&mut self, frame: &mut Frame<'_>, area: Rect) {
        // Does nothing by default.
    }
}

pub mod prelude {
    pub use super::{Component, ComponentEvent, KeyCode, TerminalEvent};
}