/// Default foreground color.
pub const FG_DEFAULT: &str = "\x1B[0m";

/// Info level foreground color.
pub const FG_INFO: &str = "\x1B[1;34m";
/// Warning level foreground color.
pub const FG_WARN: &str = "\x1B[1;33m";
/// Error level foreground color.
pub const FG_ERROR: &str = "\x1B[1;31m";