pub trait IteratorExt: Iterator {
    fn join(&mut self, separator: &str) -> String
        where Self::Item: AsRef<str> {
        let mut result = String::new();
        if let Some(value) = self.next() {
            result.push_str(value.as_ref());
        }
        for value in self {
            result.push_str(separator);
            result.push_str(value.as_ref());
        }
        result
    }
}

impl<T> IteratorExt for T
    where T: Iterator + ?Sized {}

pub mod fs_ext {
    use std::{fs, io};
    use std::path::{Path, PathBuf};

    use walkdir::WalkDir;

    pub fn read_dir_all(path: impl AsRef<Path>) -> impl Iterator<Item=io::Result<PathBuf>> {
        WalkDir::new(path)
            .into_iter()
            .map(|it| it.map(|it| it.path().to_owned()))
            .map(|it| it.map_err(|it| it.into()))
    }

    pub fn copy_dir_all(from: impl AsRef<Path>, to: impl AsRef<Path>) -> io::Result<()> {
        let from = from.as_ref();
        let to = to.as_ref();

        fs::create_dir(to)?;

        for entry in fs::read_dir(from)? {
            let entry = entry?;
            let file_type = entry.file_type()?;
            if file_type.is_dir() {
                copy_dir_all(entry.path(), to.join(entry.file_name()))?;
            } else {
                fs::copy(entry.path(), to.join(entry.file_name()))?;
            }
        }

        Ok(())
    }
}