use std::{fs, io};
use std::path::{Path, PathBuf};

use serde::{Deserialize, Deserializer, Serialize, Serializer};
use thiserror::Error;

/// Editor configuration.
///
/// This struct describes how to use an editor in Miu. Defaults the to file system explorer.
#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase", deny_unknown_fields)]
pub struct Editor {
    /// Name.
    ///
    /// Used for identification and display purposes.
    name: String,
    /// Files supported by this editor.
    ///
    /// Used to detect if a submission can be opened by this editor.  Can be a glob (like `*.txt`).
    /// If editor is file based, make sure all listed files can be used to open a submission.
    #[serde(default)]
    files: Vec<Pattern>,
    /// Temporary files produced by this editor.
    ///
    /// Used for filtering what could be considered "garbage" in a submission.
    /// Can be a glob (like `*.txt`).
    #[serde(default)]
    temporary_files: Vec<Pattern>,
    /// Indicate whether this editor is file based or directory based.
    ///
    /// When file based, the editor will be asked to open the file representing a submission.
    /// Otherwise, the directory will be used.
    #[serde(default)]
    file_based: bool,
    /// Path to editor binary, if any.
    ///
    /// When provided, Miu will use this path to call the editor. Otherwise, it will ask the system
    /// to open the submission using the default program.
    ///
    /// This doesn't have to be a full path. If program is in `$PATH`, just use the executable name.
    #[serde(default)]
    binary: Option<PathBuf>,
}

/// Editor error.
#[derive(Debug, Error)]
pub enum EditorError {
    /// Editor program not found or available.
    #[error("editor program not found or unavailable")]
    ProgramNotFound,
    /// Unmanaged IO error.
    #[error("unmanaged IO error")]
    Io(#[from] io::Error),
}

impl Editor {
    /// Name.
    ///
    /// Used for display purposes.
    pub fn name(&self) -> &str {
        &self.name
    }

    /// Opens the provided path using this editor.
    ///
    /// This doesn't check if the file or directory is supported by the editor. However, if the editor is
    /// directory-based, and the path points to a file, then the parent of that file is opened instead.
    pub fn open<P: AsRef<Path>>(&self, path: P) -> Result<(), EditorError> {
        let mut path = path.as_ref();

        // If editor is not file based, and the path points to a file, open the parent directory instead.
        if !self.file_based && fs::metadata(path)?.is_file() {
            path = path.parent().unwrap_or(path);
        }

        // Open path.
        let result = match &self.binary {
            Some(binary) => open::with_detached(path, binary.to_str().ok_or(EditorError::ProgramNotFound)?),
            None => open::that_detached(path),
        };

        result.map_err(|_| EditorError::ProgramNotFound)
    }

    /// Checks whether this editor can open a file.
    ///
    /// This doesn't check the files contents. This only checks if the file name matches a certain pattern.
    pub fn can_open<P: AsRef<Path>>(&self, path: P) -> bool {
        self.files.iter().any(|it| it.matches(&path))
    }

    /// Indicate whether this editor is file based or directory based.
    ///
    /// When file based, the editor will be asked to open the file representing a submission.
    /// Otherwise, the directory will be used.
    pub fn file_based(&self) -> bool {
        self.file_based
    }

    /// Checks whether this editor considers a file to be a temporary file.
    ///
    /// This doesn't check the files contents. This only checks if the file path matches a certain pattern.
    pub fn is_temporary_file<P: AsRef<Path>>(&self, path: P) -> bool {
        self.temporary_files.iter().any(|it| it.matches(&path))
    }

    /// File Explorer.
    pub fn file_explorer() -> Self {
        Self {
            name: "File Explorer".into(),
            files: vec![],
            temporary_files: vec![],
            file_based: false,
            binary: None,
        }
    }

    /// Visual Studio.
    pub fn visual_studio() -> Self {
        Self {
            name: "Visual Studio".into(),
            files: vec![
                Pattern::new("*.sln").expect("pattern should be valid"),
            ],
            temporary_files: vec![
                Pattern::new("*.vs").expect("pattern should be valid"),
                Pattern::new("*.suo").expect("pattern should be valid"),
                Pattern::new("*.user").expect("pattern should be valid"),
                Pattern::new("*.userperfs").expect("pattern should be valid"),
                Pattern::new("bin").expect("pattern should be valid"),
                Pattern::new("obj").expect("pattern should be valid"),
                Pattern::new("TestResults").expect("pattern should be valid"),
            ],
            file_based: true,
            binary: None,
        }
    }

    /// Visual Studio Code.
    pub fn visual_studio_code() -> Self {
        Self {
            name: "Visual Studio Code".into(),
            files: vec![
                Pattern::new("*.html").expect("pattern should be valid"),
                Pattern::new("*.php").expect("pattern should be valid"),
                Pattern::new("*.sql").expect("pattern should be valid"),
            ],
            temporary_files: vec![],
            file_based: false,
            binary: Some("code".into()),
        }
    }

    /// PDF Reader.
    pub fn pdf_reader() -> Self {
        Self {
            name: "PDF Reader".into(),
            files: vec![
                Pattern::new("*.pdf").expect("pattern should be valid"),
            ],
            temporary_files: vec![],
            file_based: true,
            binary: None,
        }
    }

    /// Office suite editor.
    pub fn office_suite() -> Self {
        Self {
            name: "Office Suite".into(),
            files: vec![
                Pattern::new("*.docx").expect("pattern should be valid"),
                Pattern::new("*.xlsx").expect("pattern should be valid"),
            ],
            temporary_files: vec![],
            file_based: true,
            binary: None,
        }
    }
}

impl Default for Editor {
    fn default() -> Self {
        Self::file_explorer()
    }
}

/// Glob pattern.
#[derive(Debug, Clone)]
struct Pattern(glob::Pattern);

impl Pattern {
    /// Create new pattern.
    fn new(pattern: &str) -> Result<Self, glob::PatternError> {
        let pattern = glob::Pattern::new(&format!("**/{pattern}"))?;

        Ok(Self(pattern))
    }

    /// Access the original pattern.
    fn as_str(&self) -> &str {
        self.0.as_str().strip_prefix("**/").expect("pattern should begin with prefix")
    }

    /// Check if path matches pattern.
    ///
    /// Not case-sensitive.
    fn matches<P: AsRef<Path>>(&self, path: P) -> bool {
        self.0.matches_path_with(path.as_ref(), glob::MatchOptions { case_sensitive: false, ..Default::default() })
    }
}

impl Serialize for Pattern {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
        where S: Serializer {
        let pattern = self.as_str();
        serializer.serialize_str(pattern)
    }
}

impl<'de> Deserialize<'de> for Pattern {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
        where D: Deserializer<'de> {
        use serde::de::Error;

        let pattern: String = Deserialize::deserialize(deserializer)?;
        Self::new(&pattern).map_err(|e| D::Error::custom(e.to_string()))
    }
}