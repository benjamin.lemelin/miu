use std::fmt::{self, Display};
use std::fs::{self, File};
use std::io::{self, Cursor, Write};
use std::path::{Path, PathBuf};

use serde::Serialize;
use thiserror::Error;
use walkdir::WalkDir;
use zip::{result::ZipError, ZipArchive, ZipWriter};

use super::config::Config;
use super::editor::{Editor, EditorError};
use super::log::Log;
use super::submission::{EditorConfig, RegistrationNumber, Student, Submission};
use crate::util::{fs_ext, IteratorExt};

/// Miu workspace.
///
/// A workspace is a directory containing student submissions and a metadata file about those submissions.
#[derive(Debug)]
pub struct Workspace {
    path: PathBuf,
    submissions: Vec<Submission>,
}

/// Workspace errors.
#[derive(Debug, Error)]
pub enum WorkspaceError {
    /// Workspace metadata was not found.
    #[error("workspace not found or not a workspace")]
    NotFound(#[source] io::Error),
    /// Can't initialize workspace because destination is not a directory.
    #[error("destination not a directory")]
    DestinationNotADirectory(#[source] io::Error),
    /// Can't initialize workspace because destination is not empty.
    #[error("destination not empty")]
    DestinationNotEmpty,
    /// Unable to serialize or deserialize workspace metadata.
    #[error("invalid workspace metadata")]
    InvalidWorkspaceMetadata(#[from] serde_json::Error),
    /// Unable to add submission to workspace because archive is invalid or unsupported archive.
    #[error("invalid or unsupported submission archive")]
    InvalidSubmissionArchive(#[source] ZipError),
    /// Student name or registration number invalid or missing in submission name.
    #[error("missing student name or registration number in submission {0}")]
    InvalidStudentMetadata(String),
    /// Unsupported submission format.
    #[error("batch import only supports folders or zip files")]
    UnsupportedBatch,
    /// Submission not found in workspace.
    #[error("submission not found")]
    SubmissionNotFound,
    /// Teammate not found in submission.
    #[error("teammate not found in submission")]
    TeammateNotFound,
    /// Teammate is already in another team.
    #[error("teammate is already in another team")]
    TeammateAlreadyPaired,
    /// Unmanaged editor error.
    #[error("unmanaged editor error")]
    EditorError(#[source] EditorError),
    /// Unmanaged IO error.
    #[error("unmanaged IO error")]
    Io(#[from] io::Error),
}

/// Workspace log item.
#[derive(Debug, Serialize)]
enum WorkspaceLogItem {
    /// Submission was successfully added to workspace.
    SubmissionAdded(String),
    /// Submission already existed in workspace.
    SubmissionAlreadyExists(String),
    /// Invalid or unsupported submission archive.
    InvalidSubmissionArchive(String),
    /// Found path in submission that would lead outside workspace.
    UnsafeSubmission(String),
    /// Found non-utf8 path inside submission.
    InvalidPath(String),
    /// Teammate was successfully added to submission.
    TeammateAdded(String),
    /// Submission already has specified teammate.
    TeammateAlreadyExists(String),
}

impl Workspace {
    const METADATA_FOLDER: &'static str = ".miu";
    const METADATA_FILENAME: &'static str = "metadata.json";

    /// Create workspace inside a directory.
    ///
    /// Creates the directory it if it does not already exist. The directory must be empty.
    pub fn new<P: AsRef<Path>>(path: P) -> Result<Self, WorkspaceError> {
        let path = path.as_ref();

        // Create workspace directory if it does not exist.
        if let Err(e) = fs::create_dir(path) {
            match e.kind() {
                io::ErrorKind::AlreadyExists => {
                    // Destination exists. Ensure it is a directory.
                    if fs::metadata(path)?.is_file() {
                        return Err(WorkspaceError::DestinationNotADirectory(e));
                    }
                }
                _ => {
                    // Other IO error.
                    return Err(e.into());
                }
            }
        }

        // Ensure destination is empty.
        if fs::read_dir(path).and_then(|mut it| it.next().transpose())?.is_some() {
            return Err(WorkspaceError::DestinationNotEmpty);
        }

        // Create and save metadata inside workspace.
        let workspace = Self { path: path.to_owned(), submissions: Vec::new() };
        workspace.save()?;
        Ok(workspace)
    }

    /// Open workspace from directory.
    pub fn open<P: AsRef<Path>>(path: P) -> Result<Self, WorkspaceError> {
        let path = path.as_ref();

        match fs::read_to_string(path.join(Self::METADATA_FOLDER).join(Self::METADATA_FILENAME)) {
            Ok(file) => {
                Ok(Self {
                    path: path.to_owned(),
                    submissions: serde_json::from_str(&file)?,
                })
            }
            Err(e) => match e.kind() {
                io::ErrorKind::NotFound => Err(WorkspaceError::NotFound(e)),
                _ => Err(e.into())
            },
        }
    }

    /// Workspace name (derived from path).
    pub fn name(&self) -> &str {
        self.path.file_name().and_then(|it| it.to_str()).unwrap_or("<Unknown>")
    }

    /// Workspace path.
    #[allow(unused)]
    pub fn path(&self) -> &Path {
        &self.path
    }

    /// Workspace submissions.
    pub fn submissions(&self) -> &[Submission] {
        &self.submissions
    }

    /// Return true if the workspace contains no submissions.
    pub fn is_empty(&self) -> bool {
        self.submissions.is_empty()
    }

    /// Add submission to workspace.
    ///
    /// This is permanent : the workspace will be saved automatically.
    ///
    /// Supports multiple formats :
    ///  - Zip file containing one submission.
    ///  - Directory containing one submission.
    ///  - File representing a submission.
    ///
    /// Batch import is also supported with :
    ///  - Zip file containing multiple submissions.
    ///  - Directory containing multiple submissions.
    ///
    /// This will always make a copy of the original submission into the workspace. Note that
    /// multiple submissions for the same student is not allowed : this will result in a warning.
    pub fn add<P: AsRef<Path>>(&mut self, path: P, config: &Config) -> Result<Log, WorkspaceError> {
        // NOTE : Extracting everything to a temporary folder is, by far, the simplest solution. Trying to do
        // this without that first step would require to create some sort of virtual file system abstracting
        // over real files, directories and zip files (which are not `Seek` by the way).

        let mut log = Log::new();

        // Open source file (essentially locking it while we add it to the workspace).
        let path = path.as_ref();
        let mut file = File::open(path)?;
        let name = path.file_stem().and_then(|it| it.to_str()).unwrap_or("").to_owned();
        let extension = path.extension().and_then(|it| it.to_str()).map(|it| it.to_lowercase());

        // Temporary directory where all submissions to import will be copied first.
        let temp_dir = tempfile::tempdir_in(self.path.join(Self::METADATA_FOLDER))?;
        let is_batch_import = name.parse::<Student>().is_err();
        if is_batch_import {
            // Batch import. Copy all submissions inside the temp directory.
            let output_path = temp_dir.path();

            if file.metadata()?.is_dir() {
                fs_ext::copy_dir_all(path, output_path)?;
            } else if let Some("zip") = extension.as_deref() {
                ZipArchive::new(file).and_then(|mut it| it.extract(output_path))?;
            } else {
                return Err(WorkspaceError::UnsupportedBatch);
            }
        } else {
            // Single import. Copy the submission inside the temp directory.
            let output_path = temp_dir.path().join(&name);
            fs::create_dir(&output_path)?;

            if file.metadata()?.is_dir() {
                fs_ext::copy_dir_all(path, output_path)?;
            } else if let Some("zip") = extension.as_deref() {
                ZipArchive::new(file).and_then(|mut it| it.extract(output_path))?;
            } else {
                let mut output_file = File::create(output_path)?;
                io::copy(&mut file, &mut output_file)?;
            }
        }

        // Submissions to import are now inside the temporary directory. Add them all to the workspace.
        let mut submissions = Vec::new();
        let editors = config.editors();
        for entry in fs::read_dir(&temp_dir)? {
            let entry = entry?;

            let mut path = entry.path();
            let name = path.file_stem().and_then(|it| it.to_str()).unwrap_or("").to_owned();

            // Skip if submission already inside workspace.
            if self.submissions.iter().any(|it| it.name() == name) {
                log.warn(WorkspaceLogItem::SubmissionAlreadyExists(name));
                continue;
            }

            // Parse student.
            let student: Student = match name.parse() {
                Ok(student) => student,
                Err(_) => return Err(WorkspaceError::InvalidStudentMetadata(name)),
            };

            // Extract if submission is Zip archive (and delete it afterwards).
            let mut extracted = false;
            let extension = path.extension().and_then(|it| it.to_str()).map(|it| it.to_lowercase());
            if let Some("zip") = extension.as_deref() {
                let output_path = temp_dir.path().join(&name);
                match ZipArchive::new(File::open(&path)?).and_then(|mut it| it.extract(&output_path)) {
                    Ok(_) => {
                        fs::remove_file(&path)?;
                        path = output_path;
                        extracted = true;
                    }
                    Err(e) => match e {
                        ZipError::UnsupportedArchive(_) | ZipError::FileNotFound => {
                            log.warn(WorkspaceLogItem::InvalidSubmissionArchive(name.clone()));
                        }
                        ZipError::InvalidArchive(_) => {
                            log.warn(WorkspaceLogItem::UnsafeSubmission(name.clone()));
                        }
                        ZipError::Io(e) => {
                            return Err(e.into());
                        }
                    }
                }
            }

            // If not a ZIP archive, move to own folder.
            if !extracted {
                let output_path = temp_dir.path().join(&name);

                fs::create_dir(&output_path)?;
                fs::rename(&path, output_path.join(path.file_name().unwrap_or(name.as_ref())))?;
                path = output_path;
            }

            // List submission files.
            let files = fs_ext::read_dir_all(&path).collect::<io::Result<Vec<PathBuf>>>()?;

            // Find best suited editor.
            fn find_editor<'a, 'b>(
                path: &'a Path,
                prefix: &Path,
                files: &'a [PathBuf],
                editors: &'b [Editor],
                default_editor: &'b Editor,
            ) -> (&'b Editor, &'a Path) {
                // Assume default editor is the best.
                let mut result = (default_editor, path);

                // Try to find a better suited editor.
                'outer: for editor in editors {
                    for file in files {
                        if editor.can_open(file) {
                            result = (editor, file);
                            break 'outer;
                        }
                    }
                }

                // Make path relative to workspace.
                result.1 = result.1.strip_prefix(prefix).expect("file should be inside submission");
                result
            }

            // Find garbage files inside submission.
            fn find_garbage(
                prefix: &Path,
                mut files: Vec<PathBuf>,
                editor: &Editor,
            ) -> Vec<PathBuf> {
                // Temporary files are considered garbage. This is why we need to find the editor first,
                // because temporary files changes depending on the editor.
                files.retain(|it| editor.is_temporary_file(it));

                // This second pass removes children of garbage directories.
                // The dedup function assumes the array to be sorted, which is why it is sorted now (on Linux systems,
                // alphabetical order of the files is not guaranteed).
                files.sort();
                files.dedup_by(|lhs, rhs| lhs.starts_with(rhs));

                // Make paths relative to workspace.
                for file in &mut files {
                    *file = file.strip_prefix(prefix).expect("file should be inside submission").to_owned();
                }

                files
            }

            // Create and add submission to workspace.
            let (editor, editor_path) = find_editor(&path, temp_dir.path(), &files, editors, config.default_editor());
            let editor_config = EditorConfig::new(editor.name(), editor_path);
            let garbage_files = find_garbage(temp_dir.path(), files, editor);
            submissions.push(Submission::new(name, student, editor_config, garbage_files));
        }

        // Move submissions from the temporary directory to the workspace.
        for submission in submissions {
            let source = temp_dir.path().join(submission.name());
            let destination = self.path.join(submission.name());

            fs::rename(source, destination)?;
            log.info(WorkspaceLogItem::SubmissionAdded(submission.name().to_owned()));
            self.submissions.push(submission);
        }

        // Drop the temporary directory (finally deleting everything inside).
        drop(temp_dir);

        // Sort submissions by name.
        self.submissions.sort_by(|lhs, rhs| lhs.name().cmp(rhs.name()));

        // Save workspace.
        self.save()?;

        Ok(log)
    }

    /// Remove submission from workspace.
    ///
    /// This is permanent : the workspace will be saved automatically.
    pub fn remove(&mut self, index: usize) -> Result<(), WorkspaceError> {
        if index < self.submissions.len() {
            let submission = self.submissions.remove(index);
            fs::remove_dir_all(self.path.join(submission.name()))?;

            self.save()
        } else {
            Err(WorkspaceError::SubmissionNotFound)
        }
    }

    /// Add teammate to a submission.
    pub fn add_teammate(&mut self, index: usize, student: Student) -> Result<Log, WorkspaceError> {
        let mut log = Log::new();

        // Ensure the teammate is not already in another team.
        if self.submissions.iter().enumerate()
            .filter(|(i, _)| *i != index)
            .any(|(_, it)| it.has_teammate(student.registration())) {
            return Err(WorkspaceError::TeammateAlreadyPaired);
        }

        // Get submission.
        let submission = self.submissions.get_mut(index).ok_or(WorkspaceError::SubmissionNotFound)?;

        // Add teammate.
        let student_name = student.name().to_owned();
        if submission.add_teammate(student) {
            log.info(WorkspaceLogItem::TeammateAdded(student_name));
        } else {
            log.warn(WorkspaceLogItem::TeammateAlreadyExists(student_name));
        }

        Ok(log)
    }

    /// Remove teammate from a submission.
    pub fn remove_teammate(&mut self, index: usize, registration: &RegistrationNumber) -> Result<(), WorkspaceError> {
        let submission = self.submissions.get_mut(index).ok_or(WorkspaceError::SubmissionNotFound)?;

        if !submission.remove_teammate(registration) {
            return Err(WorkspaceError::TeammateNotFound);
        }

        Ok(())
    }

    /// Open all submissions using provided editor.
    pub fn open_all_submissions(
        &mut self,
        editor: &Editor,
    ) -> Result<(), WorkspaceError> {
        editor.open(&self.path)?;

        for submission in &mut self.submissions {
            submission.set_opened();
        }

        Ok(())
    }

    /// Open submission using provided editor.
    pub fn open_submission(
        &mut self,
        index: usize,
        editor: &Editor,
    ) -> Result<(), WorkspaceError> {
        let submission = self.submissions.get_mut(index).ok_or(WorkspaceError::SubmissionNotFound)?;

        editor.open(self.path.join(submission.editor_config().path()))?;
        submission.set_opened();

        Ok(())
    }

    /// Export workspace and all his submissions inside a zip archive (excluding temporary files).
    pub fn export<P: AsRef<Path>>(&self, path: P, config: &Config) -> Result<Log, WorkspaceError> {
        let mut log = Log::new();

        // Archive writer.
        let mut archive = ZipWriter::new(File::create(path)?);
        let default_editor = Editor::default();

        let mut buffer = Vec::new();
        for submission in &self.submissions {
            // Determine path to zip. We use the editor configured path, not the
            // submission path (which can have many sub folders).
            let mut path = self.path.join(submission.editor_config().path());
            if fs::metadata(&path)?.is_file() {
                if let Some(parent) = path.parent() {
                    path = parent.to_owned();
                }
            }

            // Zip all submission files, ignoring temporary files.
            let mut has_invalid_path = false;
            let mut submission_archive = ZipWriter::new(Cursor::new(buffer));
            let editor = config.find_editor(submission.editor_config().editor_name()).unwrap_or(&default_editor);
            for file in WalkDir::new(&path).into_iter().filter_entry(|it| !editor.is_temporary_file(it.path())) {
                let file = file?;

                if file.file_type().is_file() {
                    let file_path = file.path();
                    let relative_file_path = file_path.strip_prefix(&path).expect("file should be inside submission");
                    match relative_file_path.to_str() {
                        Some(relative_file_path) => {
                            submission_archive.start_file(relative_file_path, Default::default())?;
                            submission_archive.write_all(&fs::read(file_path)?)?;
                        }
                        None => {
                            has_invalid_path = true;
                        }
                    }
                }
            }

            if has_invalid_path {
                log.warn(WorkspaceLogItem::InvalidPath(submission.name().to_owned()));
            }

            buffer = submission_archive.finish()?.into_inner();

            let archive_name = submission.students().iter().map(|it| it.registration()).join("_");
            archive.start_file(format!("{}.zip", archive_name), Default::default())?;
            archive.write_all(&buffer)?;
            buffer.clear();
        }

        archive.finish()?;
        Ok(log)
    }

    /// Save workspace.
    pub fn save(&self) -> Result<(), WorkspaceError> {
        let metadata_folder_path = self.path.join(Self::METADATA_FOLDER);
        if let Err(e) = fs::create_dir(&metadata_folder_path) {
            if io::ErrorKind::AlreadyExists != e.kind() {
                return Err(e.into());
            }
        }

        let metadata_file_path = metadata_folder_path.join(Self::METADATA_FILENAME);
        fs::write(metadata_file_path, serde_json::to_string_pretty(&self.submissions)?)?;
        Ok(())
    }
}

impl From<ZipError> for WorkspaceError {
    fn from(error: ZipError) -> Self {
        match error {
            ZipError::InvalidArchive(_) | ZipError::UnsupportedArchive(_) | ZipError::FileNotFound => {
                WorkspaceError::InvalidSubmissionArchive(error)
            }
            ZipError::Io(e) => {
                WorkspaceError::Io(e)
            }
        }
    }
}

impl From<EditorError> for WorkspaceError {
    fn from(error: EditorError) -> Self {
        match error {
            EditorError::ProgramNotFound => Self::EditorError(error),
            EditorError::Io(e) => Self::Io(e)
        }
    }
}

impl From<walkdir::Error> for WorkspaceError {
    fn from(error: walkdir::Error) -> Self {
        Self::Io(error.into())
    }
}

impl Display for Workspace {
    fn fmt(&self, formatter: &mut fmt::Formatter<'_>) -> fmt::Result {
        use tabled::{settings::Style, builder::Builder};

        let mut builder = Builder::new();
        builder.push_record(["Student", "Registration", "Editor", "Open", "Garbage"]);
        for submission in &self.submissions {
            builder.push_record([
                &submission.students().iter().map(|it| it.name()).join(", "),
                &submission.students().iter().map(|it| it.registration()).join(", "),
                submission.editor_config().editor_name(),
                &submission.is_opened().to_string(),
                &submission.has_garbage().to_string(),
            ]);
        }

        let mut table = builder.index().build();
        table.with(Style::markdown());
        write!(formatter, "{}", table)
    }
}

impl Display for WorkspaceLogItem {
    fn fmt(&self, formatter: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::SubmissionAdded(name) => {
                write!(formatter, "submission `{}` added to the workspace", name)
            }
            Self::SubmissionAlreadyExists(name) => {
                write!(formatter, "submission `{}` already exists inside the workspace", name)
            }
            Self::InvalidSubmissionArchive(name) => {
                write!(formatter, "submission `{}` has an invalid zip file", name)
            }
            Self::UnsafeSubmission(name) => {
                write!(formatter, "submission `{}` was not extracted, because it contains unsafe path that would lead outside the workspace", name)
            }
            Self::InvalidPath(name) => {
                write!(formatter, "submission `{}` contains non-utf8 path, some files may be missing", name)
            }
            Self::TeammateAdded(name) => {
                write!(formatter, "teammate `{}` added to the submission", name)
            }
            Self::TeammateAlreadyExists(name) => {
                write!(formatter, "teammate `{}` already exists inside the submission", name)
            }
        }
    }
}