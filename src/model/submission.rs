use std::fmt::{self, Debug, Display, Formatter};
use std::path::{Path, PathBuf};
use std::str::FromStr;

use lazy_static::lazy_static;
use regex::Regex;
use serde::{Deserialize, Deserializer, Serialize, Serializer};
use thiserror::Error;

/// Submission.
#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase", deny_unknown_fields)]
pub struct Submission {
    name: String,
    students: Vec<Student>,
    editor_config: EditorConfig,
    garbage_files: Vec<PathBuf>,
    is_opened: bool,
}

impl Submission {
    /// Create new submission.
    pub fn new(
        name: String,
        student: Student,
        editor_config: EditorConfig,
        garbage_files: Vec<PathBuf>,
    ) -> Self {
        Self {
            name,
            students: vec![student],
            editor_config,
            garbage_files,
            is_opened: false,
        }
    }

    /// Name of the submission.
    pub fn name(&self) -> &str {
        &self.name
    }

    /// Students for this submission.
    pub fn students(&self) -> &[Student] {
        &self.students
    }

    /// Add student to submission.
    ///
    /// Returns whether the student was added.
    pub fn add_teammate(&mut self, teammate: Student) -> bool {
        if !self.has_teammate(&teammate.registration) {
            self.students.push(teammate);
            true
        } else {
            false
        }
    }

    /// Remove student from submission.
    ///
    /// Returns whether the student was removed.
    pub fn remove_teammate(&mut self, registration: &RegistrationNumber) -> bool {
        match self.students.iter().position(|it| it.registration == *registration) {
            Some(index) => {
                self.students.remove(index);
                true
            }
            None => {
                false
            }
        }
    }

    /// Check if submission has specified student in it.
    pub fn has_teammate(&self, registration: &RegistrationNumber) -> bool {
        self.students.iter().any(|it| it.registration == *registration)
    }

    /// Editor configuration to open this submission.
    pub fn editor_config(&self) -> &EditorConfig {
        &self.editor_config
    }

    /// Indicates if submission contains garbage files.
    pub fn has_garbage(&self) -> bool {
        !self.garbage_files.is_empty()
    }

    /// Indicates if this submission has been opened.
    pub fn is_opened(&self) -> bool {
        self.is_opened
    }

    /// Mark submission as opened.
    pub fn set_opened(&mut self) {
        self.is_opened = true;
    }
}

/// Student.
#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase", deny_unknown_fields)]
pub struct Student {
    name: String,
    registration: RegistrationNumber,
}

impl Student {
    /// Create new student.
    pub fn new(
        name: impl Into<String>,
        registration: impl Into<RegistrationNumber>,
    ) -> Self {
        Self {
            name: name.into(),
            registration: registration.into(),
        }
    }

    /// Student name.
    pub fn name(&self) -> &str {
        &self.name
    }

    /// Student registration number.
    pub fn registration(&self) -> &RegistrationNumber {
        &self.registration
    }
}

impl Eq for Student {}

impl PartialEq for Student {
    fn eq(&self, other: &Self) -> bool {
        self.registration.eq(&other.registration)
    }
}

/// Student errors.
#[derive(Debug, Error)]
pub enum StudentError {
    /// Unable to parse student name or registration number from metadata.
    #[error("missing student name or registration number in metadata")]
    InvalidMetadata,
}

impl FromStr for Student {
    type Err = StudentError;

    fn from_str(value: &str) -> Result<Self, Self::Err> {
        lazy_static! {
            static ref METADATA_REGEX : Regex = Regex::new(r"([^\d]+)_(\d{7})").unwrap();
        }

        match METADATA_REGEX.captures(value) {
            Some(captures) => {
                let name = captures.get(1)
                    .expect("capture 1 should exist")
                    .as_str()
                    .replace('_', " ");
                let registration = captures.get(2)
                    .expect("capture 2 should exist")
                    .as_str()
                    .parse::<RegistrationNumber>()
                    .expect("regex should have validated the registration number");

                Ok(Self::new(name, registration))
            }
            None => {
                Err(Self::Err::InvalidMetadata)
            }
        }
    }
}

/// Editor configuration.
#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase", deny_unknown_fields)]
pub struct EditorConfig {
    editor_name: String,
    path: PathBuf,
}

impl EditorConfig {
    /// Create new editor configuration.
    pub fn new(
        editor_name: impl Into<String>,
        path: impl Into<PathBuf>,
    ) -> Self {
        Self {
            editor_name: editor_name.into(),
            path: path.into(),
        }
    }

    /// Editor name.
    pub fn editor_name(&self) -> &str {
        &self.editor_name
    }

    /// Path to open by the editor.
    pub fn path(&self) -> &Path {
        &self.path
    }
}

/// Registration number.
#[derive(Debug, Clone, Eq, PartialEq)]
pub struct RegistrationNumber(String);

/// Registration number error.
#[derive(Debug, Error)]
pub enum RegistrationNumberError {
    /// Value is not 7 characters longs.
    #[error("registration number is not 7 characters long")]
    InvalidLength,
    /// Value contains a non digit character.
    #[error("invalid digit found in registration number")]
    InvalidDigit,
}

impl RegistrationNumber {
    /// Create new registration number from a string.
    pub fn new(
        value: impl Into<String>
    ) -> Result<Self, RegistrationNumberError> {
        let value = value.into();

        if value.len() != 7 {
            Err(RegistrationNumberError::InvalidLength)
        } else if value.chars().any(|it| !it.is_ascii_digit()) {
            Err(RegistrationNumberError::InvalidDigit)
        } else {
            Ok(Self(value))
        }
    }
}

impl AsRef<str> for RegistrationNumber {
    fn as_ref(&self) -> &str {
        self.0.as_ref()
    }
}

impl FromStr for RegistrationNumber {
    type Err = RegistrationNumberError;

    fn from_str(value: &str) -> Result<Self, Self::Err> {
        Self::new(value)
    }
}

impl Display for RegistrationNumber {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "{}", &self.0)
    }
}

impl Serialize for RegistrationNumber {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
        where S: Serializer {
        serializer.serialize_str(&self.0)
    }
}

impl<'de> Deserialize<'de> for RegistrationNumber {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
        where D: Deserializer<'de> {
        use serde::de::Error;

        let value: String = Deserialize::deserialize(deserializer)?;
        Self::new(value).map_err(|e| D::Error::custom(e.to_string()))
    }
}