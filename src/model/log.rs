/// Log.
///
/// Represents a journal. Unlike regular logs, this is intended to be returned by other functions
/// alongside their result. These logs may be printed afterward in a pretty and concise fashion.
#[derive(Debug, Default)]
pub struct Log {
    pub info: Vec<String>,
    pub warnings: Vec<String>,
}

impl Log {
    /// Create new empty log.
    pub fn new() -> Self {
        Self::default()
    }

    /// Add info to the log.
    pub fn info<T: ToString>(&mut self, item: T) {
        self.info.push(item.to_string());
    }

    /// Add warning to the log.
    pub fn warn<T: ToString>(&mut self, item: T) {
        self.warnings.push(item.to_string());
    }

    /// Append other log to this one.
    pub fn append(&mut self, mut other: Self) {
        self.info.append(&mut other.info);
        self.warnings.append(&mut other.warnings);
    }
}