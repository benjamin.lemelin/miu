use std::{fs, io};
use std::iter;
use std::path::PathBuf;

use directories::ProjectDirs;
use lazy_static::lazy_static;
use serde::{Deserialize, Serialize};
use thiserror::Error;

use super::editor::Editor;

/// Miu configuration.
///
/// Configures how Miu handles submissions and editors for operations like extracting an archive,
/// initializing a workspace or opening a submission.
#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase", deny_unknown_fields)]
pub struct Config {
    #[serde(default)]
    editors: Vec<Editor>,
    #[serde(default)]
    default_editor: Editor,
}

/// Config errors.
#[derive(Debug, Error)]
pub enum ConfigError {
    /// Config file was not found.
    #[error("config file not found")]
    NotFound(#[source] io::Error),
    /// Unable to serialize or deserialize config.
    #[error("invalid config")]
    InvalidConfig(#[from] serde_json::Error),
    /// Invalid config file format.
    #[error("unable to determine data directory location (usually `$HOME/.config` or `%APPDATA%/Roaming`)")]
    UndeterminedDataDirectory,
    /// Unmanaged IO error.
    #[error("unmanaged IO error")]
    Io(#[from] io::Error),
}

lazy_static! {
    static ref PROJECT_DIRS : Option<ProjectDirs> = ProjectDirs::from("ca", "blemelin", "miu");
}

impl Config {
    const CONFIG_FILENAME: &'static str = "miu.json";

    /// Return the full path to the config file.
    ///
    /// Config file may not exist at that location.
    pub fn path() -> Result<PathBuf, ConfigError> {
        let dirs = PROJECT_DIRS.as_ref().ok_or(ConfigError::UndeterminedDataDirectory)?;
        let path = dirs.config_dir().join(Self::CONFIG_FILENAME);

        Ok(path)
    }

    /// Load config.
    ///
    /// May result in an error (most likely IO related).
    pub fn load() -> Result<Self, ConfigError> {
        // We are handling the IO ourselves, so any error from serde_yaml has to
        // be a serializing or a deserializing issue.
        match fs::read_to_string(Self::path()?) {
            Ok(file) => Ok(serde_json::from_str(&file)?),
            Err(e) if e.kind() == io::ErrorKind::NotFound => Err(ConfigError::NotFound(e)),
            Err(e) => Err(ConfigError::Io(e))
        }
    }

    /// Editors configurations (excluding default one).
    ///
    /// Configures how Miu analyse and open submissions. For example, it can be used to determine
    /// the nature of a submission (what kind of project it is).
    pub fn editors(&self) -> &[Editor] {
        &self.editors
    }

    /// Default editor to use when opening a submission of unknown nature or opening multiple
    /// submissions at once.
    ///
    /// Defaults to the systems file explorer.
    pub fn default_editor(&self) -> &Editor {
        &self.default_editor
    }

    /// All editors configurations (including the default one).
    ///
    /// Produce an iterator.
    pub fn all_editors(&self) -> impl Iterator<Item=&Editor> {
        self.editors
            .iter()
            .chain(iter::once(&self.default_editor))
    }

    /// Find editor by name (including default one).
    ///
    /// Returns none if the requested editor was not found.
    pub fn find_editor(&self, name: &str) -> Option<&Editor> {
        self.all_editors().find(|it| it.name() == name)
    }

    /// Save config.
    ///
    /// May result in an error (most likely IO related).
    pub fn save(&self) -> Result<(), ConfigError> {
        let dirs = PROJECT_DIRS.as_ref().ok_or(ConfigError::UndeterminedDataDirectory)?;
        let dir = dirs.config_dir();
        let path = dir.join(Self::CONFIG_FILENAME);

        fs::create_dir_all(dir)?;
        fs::write(path, serde_json::to_string_pretty(&self)?)?;

        Ok(())
    }
}

impl Default for Config {
    fn default() -> Self {
        Self {
            editors: vec![
                Editor::visual_studio(),
                Editor::visual_studio_code(),
                Editor::pdf_reader(),
                Editor::office_suite(),
            ],
            default_editor: Default::default(),
        }
    }
}