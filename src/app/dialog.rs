use ratatui::{prelude::*, widgets::*};

use crate::tui::prelude::*;

#[derive(Debug)]
pub struct EditorSelectDialog {
    editors: Vec<String>,
    view: EditorSelectDialogView,
}

#[derive(Debug)]
struct EditorSelectDialogView {
    list: ListState,
    scrollbar: ScrollbarState,
}

#[derive(Debug)]
pub enum EditorSelectDialogEvent {
    Ok,
    Cancel,
}

impl EditorSelectDialog {
    pub fn new(recommended_editor: String, other_editors: Vec<String>) -> Self {
        let mut editors = other_editors;
        editors.insert(0, recommended_editor);

        let nb_elements = editors.len();
        Self {
            editors,
            view: EditorSelectDialogView {
                list: ListState::default().with_selected(Some(0)),
                scrollbar: ScrollbarState::new(nb_elements),
            },
        }
    }

    pub fn selected_editor(&self) -> &str {
        &self.editors[self.view.selected_index()]
    }

    fn select_next(&mut self) {
        let new_index = self.view.selected_index() + 1;
        if new_index < self.editors.len() {
            self.view.select(new_index);
        }
    }

    fn select_previous(&mut self) {
        let index = self.view.selected_index();
        if index > 0 {
            self.view.select(index - 1);
        }
    }
}

impl EditorSelectDialogView {
    fn selected_index(&self) -> usize {
        self.list.selected().unwrap_or(0)
    }

    fn select(&mut self, index: usize) {
        self.list.select(Some(index));
        self.scrollbar = self.scrollbar.position(index);
    }
}

impl Component for EditorSelectDialog {
    type Event = EditorSelectDialogEvent;

    fn update(&mut self, event: &TerminalEvent) -> Option<Self::Event> {
        match event {
            TerminalEvent::KeyPress(KeyCode::Enter) => {
                return Some(Self::Event::Ok);
            }
            TerminalEvent::KeyPress(KeyCode::Esc) | TerminalEvent::KeyPress(KeyCode::Char('q')) => {
                return Some(Self::Event::Cancel);
            }
            TerminalEvent::KeyPress(KeyCode::Down) => {
                self.select_next()
            }
            TerminalEvent::KeyPress(KeyCode::Up) => {
                self.select_previous()
            }
            _ => {}
        }
        None
    }

    fn draw(&mut self, frame: &mut Frame<'_>, area: Rect) {
        const WIDTH: u16 = 48;
        const HEIGHT: u16 = 8;
        const BACKGROUND_STYLE: Style = Style::new().bg(Color::from_u32(0x333333));
        const SELECTED_STYLE: Style = Style::new().bg(Color::DarkGray).add_modifier(Modifier::BOLD);

        /*
         * Layout
         */
        let layout = Layout::vertical([
            Constraint::Fill(1),
            Constraint::Length(HEIGHT),
            Constraint::Fill(1)
        ]).split(area);

        let area = Layout::horizontal([
            Constraint::Fill(1),
            Constraint::Length(WIDTH),
            Constraint::Fill(1)
        ]).split(layout[1])[1];

        /*
         * Main Content
         */
        let mut editors = self.editors.clone();
        editors[0].push_str(" (Recommended)");

        let list = List::new(editors)
            .block(Block::new().title("Select Editor").borders(Borders::ALL))
            .style(BACKGROUND_STYLE)
            .highlight_style(SELECTED_STYLE);

        frame.render_widget(Clear, area);
        frame.render_stateful_widget(list, area, &mut self.view.list);

        /*
         * Scrollbar
         */
        let scrollbar = Scrollbar::new(ScrollbarOrientation::VerticalRight);
        frame.render_stateful_widget(scrollbar, area, &mut self.view.scrollbar);
    }
}