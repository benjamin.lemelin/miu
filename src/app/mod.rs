use ratatui::{prelude::*, widgets::*};
use std::iter;
use tui_input::{Input, InputRequest};

use crate::app::dialog::{EditorSelectDialog, EditorSelectDialogEvent};
use crate::model::{config::Config, editor::Editor, workspace::{Workspace, WorkspaceError}};
use crate::tui::prelude::*;
use crate::util::IteratorExt;

mod dialog;

#[derive(Debug)]
pub struct App<'a> {
    workspace: &'a mut Workspace,
    config: &'a mut Config,
    view: AppView,
}

#[derive(Debug)]
struct AppView {
    table: TableState,
    scrollbar: ScrollbarState,
    input: Option<Input>,
    error: Option<String>,
    editor_select_dialog: Option<EditorSelectDialog>,
    import: bool,
    open_all: bool,
}

impl<'a> App<'a> {
    pub fn new(workspace: &'a mut Workspace, config: &'a mut Config) -> Self {
        let nb_elements = workspace.submissions().len();

        Self {
            workspace,
            config,
            view: AppView {
                table: TableState::new().with_selected(0),
                scrollbar: ScrollbarState::new(nb_elements),
                input: None,
                error: None,
                editor_select_dialog: None,
                import: false,
                open_all: false,
            },
        }
    }

    fn select_next(&mut self) {
        let new_index = self.view.selected_submission_index() + 1;
        if new_index < self.workspace.submissions().len() {
            self.view.select(new_index);
        }
    }

    fn select_previous(&mut self) {
        let index = self.view.selected_submission_index();
        if index > 0 {
            self.view.select(index - 1);
        }
    }

    fn open_selected(&mut self) {
        if self.workspace.is_empty() {
            self.view.show_error("workspace is empty");
            return;
        }

        let submission = self.view.selected_submission_index();
        let editor = self.view.selected_editor_name().expect("dialog should be opened");

        let editor = self.config.find_editor(editor).expect("editor should exist");
        if let Err(e) = self.workspace.open_submission(submission, editor) {
            self.view.show_workspace_error(e);
        }
    }

    fn open_all(&mut self) {
        if self.workspace.is_empty() {
            self.view.show_error("workspace is empty");
            return;
        }

        let editor = self.view.selected_editor_name().expect("dialog should be opened");

        let editor = self.config.find_editor(editor).expect("editor should exist");
        if let Err(e) = self.workspace.open_all_submissions(editor) {
            self.view.show_workspace_error(e);
        }
    }

    fn delete_selected(&mut self) {
        if self.workspace.is_empty() {
            self.view.show_error("workspace is empty");
            return;
        }

        let index = self.view.selected_submission_index();
        match self.workspace.remove(index) {
            Ok(_) => {
                // Move selection down in case we deleted the last one.
                if index == self.workspace.submissions().len() {
                    self.select_previous();
                }
            }
            Err(e) => {
                self.view.show_workspace_error(e)
            }
        }
    }

    fn import(&mut self) {
        let path = self.view.input_text().expect("input should exist");

        if let Err(e) = self.workspace.add(path, self.config) {
            self.view.show_workspace_error(e);
        }
    }

    fn export(&mut self) {
        let path = self.view.input_text().expect("input should exist");

        if let Err(e) = self.workspace.export(path, self.config) {
            self.view.show_workspace_error(e);
        }
    }

    fn show_open_dialog(&mut self) {
        if self.workspace.is_empty() {
            self.view.show_error("workspace is empty");
            return;
        }

        let index = self.view.selected_submission_index();
        let submission = &self.workspace.submissions()[index];
        let editor = submission.editor_config().editor_name();

        let recommended_editor = self.config.find_editor(editor).unwrap_or(self.config.default_editor());
        let default_editor = self.config.default_editor();
        let other_editors = self.config.editors();
        self.view.show_editor_selection(recommended_editor, default_editor, other_editors, false);
    }

    fn show_open_all_dialog(&mut self) {
        if self.workspace.is_empty() {
            self.view.show_error("workspace is empty");
            return;
        }

        let recommended_editor = self.config.default_editor();
        let default_editor = self.config.default_editor();
        let other_editors = self.config.editors();
        self.view.show_editor_selection(recommended_editor, default_editor, other_editors, true);
    }

    fn hide_dialog(&mut self) {
        self.view.hide_editor_selection();
    }

    fn show_import_input(&mut self) {
        self.view.show_input(true);
    }

    fn show_export_input(&mut self) {
        self.view.show_input(false);
    }

    fn hide_input(&mut self) {
        self.view.hide_input();
    }
}

impl AppView {
    fn selected_submission_index(&self) -> usize {
        self.table.selected().unwrap_or(0)
    }

    fn select(&mut self, index: usize) {
        self.table.select(Some(index));
        self.scrollbar = self.scrollbar.position(index);
    }

    fn show_input(&mut self, import: bool) {
        self.input = Some(Input::new(if import { "" } else { "Travaux.zip" }.into()));
        self.import = import;
    }

    fn hide_input(&mut self) {
        self.input = None;
    }

    fn input_text(&self) -> Option<&str> {
        self.input.as_ref().map(|it| it.value())
    }

    fn show_editor_selection(
        &mut self,
        recommended_editor: &Editor,
        default_editor: &Editor,
        other_editors: &[Editor],
        open_all: bool,
    ) {
        self.editor_select_dialog = Some(EditorSelectDialog::new(
            recommended_editor.name().to_owned(),
            other_editors
                .iter()
                .chain(iter::once(default_editor))
                .filter(|it| it.name() != recommended_editor.name())
                .filter(|it| !open_all || !it.file_based())
                .map(|it| it.name().to_owned()).
                collect(),
        ));
        self.open_all = open_all;
    }

    fn hide_editor_selection(&mut self) {
        self.editor_select_dialog = None;
    }

    fn selected_editor_name(&self) -> Option<&str> {
        self.editor_select_dialog.as_ref().map(|it| it.selected_editor())
    }

    fn show_error(&mut self, error: &str) {
        self.error = Some(format!("Error: {error}."));
    }

    fn show_workspace_error(&mut self, error: WorkspaceError) {
        self.show_error(&match error {
            WorkspaceError::EditorError(error) => error.to_string(),
            _ => error.to_string()
        });
    }
}

impl<'a> Component for App<'a> {
    type Event = ComponentEvent;

    fn update(&mut self, event: &TerminalEvent) -> Option<Self::Event> {
        // Pass events to current dialog, if any.
        if let Some(dialog) = &mut self.view.editor_select_dialog {
            if let Some(event) = dialog.update(event) {
                if let EditorSelectDialogEvent::Ok = event {
                    if self.view.open_all {
                        self.open_all();
                    } else {
                        self.open_selected();
                    }
                }
                self.hide_dialog();
            }
            return None;
        }

        // Pass events to input, if any.
        if let Some(input) = &mut self.view.input {
            match event {
                TerminalEvent::KeyPress(KeyCode::Esc) => {
                    self.hide_input();
                }
                TerminalEvent::KeyPress(KeyCode::Char(char)) => {
                    input.handle(InputRequest::InsertChar(*char));
                }
                TerminalEvent::KeyPress(KeyCode::Left) => {
                    input.handle(InputRequest::GoToPrevChar);
                }
                TerminalEvent::KeyPress(KeyCode::Right) => {
                    input.handle(InputRequest::GoToNextChar);
                }
                TerminalEvent::KeyPress(KeyCode::Backspace) => {
                    input.handle(InputRequest::DeletePrevChar);
                }
                TerminalEvent::KeyPress(KeyCode::Delete) => {
                    input.handle(InputRequest::DeleteNextChar);
                }
                TerminalEvent::KeyPress(KeyCode::Enter) => {
                    if self.view.import {
                        self.import();
                    } else {
                        self.export();
                    }
                    self.hide_input();
                }
                _ => {}
            }
            return None;
        }

        // Otherwise, handle event.
        match event {
            TerminalEvent::KeyPress(KeyCode::Char('q')) => {
                return Some(Self::Event::Exit);
            }
            TerminalEvent::KeyPress(KeyCode::Down) => {
                self.select_next()
            }
            TerminalEvent::KeyPress(KeyCode::Up) => {
                self.select_previous()
            }
            TerminalEvent::KeyPress(KeyCode::Enter) => {
                self.show_open_dialog()
            }
            TerminalEvent::KeyPress(KeyCode::Char('o')) => {
                self.show_open_all_dialog()
            }
            TerminalEvent::KeyPress(KeyCode::Char('i')) => {
                self.show_import_input()
            }
            TerminalEvent::KeyPress(KeyCode::Char('e')) => {
                self.show_export_input()
            }
            TerminalEvent::KeyPress(KeyCode::Char('d')) => {
                self.delete_selected()
            }
            _ => {}
        }
        None
    }

    fn draw(&mut self, frame: &mut Frame<'_>, area: Rect) {
        const BORDER_STYLE: Style = Style::new().fg(Color::DarkGray);
        const MUTED_STYLE: Style = Style::new().fg(Color::Gray);
        const PROMPT_STYLE: Style = Style::new().fg(Color::LightGreen);
        const ERRORS_STYLE: Style = Style::new().fg(Color::Red).add_modifier(Modifier::BOLD);
        const SELECTED_STYLE: Style = Style::new().bg(Color::DarkGray).add_modifier(Modifier::BOLD);

        let submissions = self.workspace.submissions();

        /*
         * Layout
         */
        let layout = Layout::vertical([
            Constraint::Length(3), // Header.
            Constraint::Fill(1),   // Submission table.
            Constraint::Length(1), // Errors.
            Constraint::Length(3), // Footer.
        ]).split(area);

        /*
         * Header
         */
        let rows = [Row::new(["#", "", "Student", "Registration", "Editor", ""])];
        let cols = [
            Constraint::Length(4),
            Constraint::Length(2),
            Constraint::Fill(1),
            Constraint::Length(20),
            Constraint::Length(18),
            Constraint::Length(4),
        ];
        let header = Table::new(rows, cols)
            .block(Block::new().borders(Borders::TOP | Borders::BOTTOM).border_style(BORDER_STYLE));
        frame.render_widget(header, layout[0]);

        /*
         * Submission list
         */
        let rows = submissions.iter().enumerate().map(|(i, it)|
            Row::new([
                (i + 1).to_string(),
                if it.is_opened() { "✓" } else { "" }.to_owned(),
                it.students().iter().map(|it| it.name()).join(", "),
                it.students().iter().map(|it| it.registration()).join(", "),
                it.editor_config().editor_name().to_owned(),
                if it.has_garbage() { "⚠" } else { "" }.to_owned(),
            ])
        );

        let table = Table::new(rows, cols)
            .block(Block::new().borders(Borders::BOTTOM).border_style(BORDER_STYLE))
            .highlight_style(SELECTED_STYLE);
        frame.render_stateful_widget(table, layout[1], &mut self.view.table);

        let scrollbar = Scrollbar::new(ScrollbarOrientation::VerticalRight);
        frame.render_stateful_widget(scrollbar, layout[1], &mut self.view.scrollbar);

        /*
         * Input or error
         */
        match &self.view.input {
            None => {
                let error = Paragraph::new(self.view.error.take().unwrap_or_default()).style(ERRORS_STYLE);
                frame.render_widget(error, layout[2]);
            }
            Some(input) => {
                const IMPORT_PROMPT: &str = "Source : ";
                const EXPORT_PROMPT: &str = "Destination : ";

                let prompt = if self.view.import { IMPORT_PROMPT } else { EXPORT_PROMPT };
                let text = Paragraph::new(
                    Line::from(vec![Span::styled(prompt, PROMPT_STYLE), Span::from(input.value())])
                );
                frame.render_widget(text, layout[2]);
                frame.set_cursor(layout[2].x + prompt.len() as u16 + input.cursor() as u16, layout[2].y);
            }
        }

        /*
         * Footer
         */
        let workspace_name = self.workspace.name();
        let submission_count = self.workspace.submissions().len();
        let selected_index = (self.view.selected_submission_index() + 1).clamp(0, submission_count);
        let status = format!("{} [Submission {}/{}]", workspace_name, selected_index, submission_count);

        let footer = Paragraph::new(vec![
            status.into(),
            "[Q : Quit] [↓↑ : Move] [↵ : Open] [O : Open all]".into(),
            "[I : Import] [E : Export] [D : Delete]".into(),
        ]).style(MUTED_STYLE);
        frame.render_widget(footer, layout[3]);

        /*
         * Dialog
         */
        if let Some(dialog) = &mut self.view.editor_select_dialog {
            dialog.draw(frame, area);
        }
    }
}