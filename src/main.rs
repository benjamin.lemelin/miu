use std::path::Path;
use eyre::{OptionExt, Result};

use crate::app::App;
use crate::args::{Args, Command, ConfigArgs, InitArgs, SubmissionsArgs};
use crate::model::{config::{Config, ConfigError}, log::Log, workspace::Workspace};
use crate::model::submission::Student;
use crate::tui::{Tui, color};

mod app;
mod args;
mod model;
mod tui;
mod util;

fn main() {
    let args = Args::parse();
    match run(args) {
        Ok(log) => {
            for info in log.info {
                println!("{}Info{}: {}", color::FG_INFO, color::FG_DEFAULT, info);
            }
            for warning in log.warnings {
                eprintln!("{}Warning{}: {}", color::FG_WARN, color::FG_DEFAULT, warning);
            }
        }
        Err(e) => {
            eprintln!("{}Error{}: {}", color::FG_ERROR, color::FG_DEFAULT, e);
        }
    }
}

fn run(args: Args) -> Result<Log> {
    // Log.
    let mut log = Log::new();

    // Open config file in default editor, if requested. This has to be done before we read it, in
    // case the config is invalid and the use want's to fix it.
    if let Some(Command::Config(ConfigArgs { reset: false })) = &args.command {
        open::that(Config::path()?)?;
        return Ok(log);
    }

    // Load config (using default if not found).
    let mut config = Config::load().or_else(|e| match e {
        ConfigError::NotFound(_) => {
            log.info("config not found, creating default one");
            Ok(Config::default())
        }
        _ => Err(e)
    })?;

    // Open or initialize workspace.
    let mut workspace = if let Some(Command::Init(InitArgs { path, archive })) = &args.command {
        let path = path.as_deref().unwrap_or(Path::new("."));
        let mut workspace = Workspace::new(path)?;

        // The workspace can be populated at initialization.
        if let Some(archive) = archive {
            log.append(workspace.add(archive, &config)?);
        }

        workspace
    } else {
        // User is allowed to open the current directory as a workspace or
        // to input a path instead of a command.
        let path = args.path.as_deref().unwrap_or(Path::new("."));
        Workspace::open(path)?
    };

    // Perform requested operation, if any. Otherwise, open workspace in terminal ui.
    match args.command {
        None | Some(Command::Init(_)) => {
            Tui::init();
            Tui::run(App::new(&mut workspace, &mut config))?;
        }
        Some(command) => match command {
            Command::Submissions(command) => match command {
                SubmissionsArgs::Ls => {
                    println!("{workspace}");
                }
                SubmissionsArgs::Add { path } => {
                    log.append(workspace.add(&path, &config)?);
                }
                SubmissionsArgs::Remove { index } => {
                    workspace.remove(index)?;
                }
                SubmissionsArgs::AddTeammate { submission, name, registration } => {
                    log.append(workspace.add_teammate(submission, Student::new(name, registration))?);
                }
                SubmissionsArgs::RemoveTeammate { submission, registration } => {
                    workspace.remove_teammate(submission, &registration)?;
                }
                SubmissionsArgs::Open { submission: None, editor: None } => {
                    let editor = config.default_editor();
                    workspace.open_all_submissions(editor)?;
                }
                SubmissionsArgs::Open { submission: None, editor: Some(editor) } => {
                    let editor = config.find_editor(&editor).ok_or_eyre("editor not found")?;
                    workspace.open_all_submissions(editor)?;
                }
                SubmissionsArgs::Open { submission: Some(submission), editor: None } => {
                    let editor = config.default_editor();
                    workspace.open_submission(submission, editor)?;
                }
                SubmissionsArgs::Open { submission: Some(submission), editor: Some(editor) } => {
                    let editor = config.find_editor(&editor).ok_or_eyre("editor not found")?;
                    workspace.open_submission(submission, editor)?;
                }
            },
            Command::Export(command) => {
                log.append(workspace.export(command.path, &config)?);
            }
            Command::Config(ConfigArgs { reset: true }) => {
                config = Config::default()
            }
            _ => {}
        },
    }

    // Save workspace and config before exiting.
    workspace.save()?;
    config.save()?;

    Ok(log)
}