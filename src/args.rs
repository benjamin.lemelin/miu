use std::path::PathBuf;

use crate::model::submission::RegistrationNumber;

/// Miu is a toolkit designed to assist computer science teachers in their everyday tasks.
#[derive(Debug, clap::Parser)]
#[command(version, args_conflicts_with_subcommands = true)]
pub struct Args {
    /// Path to the workspace.
    pub path: Option<PathBuf>,
    /// Command.
    #[command(subcommand)]
    pub command: Option<Command>,
}

/// Miu Commands.
#[derive(Debug, clap::Subcommand)]
pub enum Command {
    /// Create new workspace.
    Init(InitArgs),
    /// Manage submissions.
    #[command(subcommand)]
    Submissions(SubmissionsArgs),
    /// Export workspace to a zip file, omitting temporary files.
    Export(ExportArgs),
    /// Open the configuration file.
    Config(ConfigArgs),
}

/// Init command args.
#[derive(Debug, clap::Args)]
pub struct InitArgs {
    /// Path to the workspace.
    pub path: Option<PathBuf>,
    /// Path to an archive to extract when initialising workspace.
    #[arg(short, long)]
    pub archive: Option<PathBuf>,
}

/// Submissions command args.
#[derive(Debug, clap::Subcommand)]
pub enum SubmissionsArgs {
    /// List all submissions.
    Ls,
    /// Add one or multiple submissions.
    Add {
        /// Path to the submission(s) to add.
        path: PathBuf,
    },
    /// Remove a submission.
    Remove {
        /// Index of to the submission to remove.
        index: usize,
    },
    /// Add teammate to submission.
    AddTeammate {
        /// Index of to the submission.
        submission: usize,
        /// Teammate name.
        name: String,
        /// Teammate registration number.
        registration: RegistrationNumber,
    },
    /// Remove teammate from submission.
    RemoveTeammate {
        /// Index of to the submission.
        submission: usize,
        /// Teammate registration number.
        registration: RegistrationNumber,
    },
    /// Open one or all submission in an editor.
    Open {
        /// Index of to the submission.
        submission: Option<usize>,
        /// Override the editor to use.
        #[arg(long)]
        editor: Option<String>,
    },
}

/// Export command args.
#[derive(Debug, clap::Args)]
pub struct ExportArgs {
    /// Zip file path.
    pub path: PathBuf,
}

/// Config comamnd args.
#[derive(Debug, clap::Args)]
pub struct ConfigArgs {
    /// Reset the config to the default values.
    #[arg(short, long)]
    pub reset: bool,
}

impl Args {
    /// Parse CLI arguments.
    pub fn parse() -> Self {
        clap::Parser::parse()
    }
}