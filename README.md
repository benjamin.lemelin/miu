<div align="center">

# ![Miu](.docs/icon-large.svg)

</div>

Miu is a toolkit designed to assist computer science teachers in their everyday tasks. It is particularly useful when
it comes to managing archives from LÉA.

She also likes dirty things... maybe even a bit too much?

![Démonstration](.docs/demo.gif)

## Installation

This project requires the [Rust] toolchain to be installed. Windows and Linux systems are both tested and supported.
All you have to do is clone the repository and run this command :

```shell
cargo install --path .
```

## Usage

Miu works somewhat like `git`. First, create a workspace using `miu init` (or `miu ./your-folder init`). Then, you should
be able to import a *LEA Archive*, examine the submissions it contains, and finally export back to an archive. 

You may also fully use it from the command line.

```text
Miu is a toolkit designed to assist computer science teachers in their everyday tasks

Usage: miu.exe [PATH]
       miu.exe <COMMAND>

Commands:
  init         Create new workspace
  submissions  Manage submissions
  export       Export workspace to a zip file, omitting temporary files
  config       Open the configuration file
  help         Print this message or the help of the given subcommand(s)

Arguments:
  [PATH]  Path to the workspace

Options:
  -h, --help     Print help
  -V, --version  Print version
```

## Configuration

Use the `miu config` command to open the configuration file inside your default editor. There, you can add/remove 
editors. Some are already configured by default (Miu will create one if it doesn't exist).

Here's what each parameter mean :

* `name`: Editor name. Used for identification and display purposes.
* `files`: Files supported by this editor. Can be a glob (like `*.txt`). If the editor is file based, make sure the
listed files represents the submissions (like `.sln` files).
* `temporaryFiles`: Temporary files produced by this editor. Used for filtering what could be considered "garbage" in a 
submission. Can be a glob (like `*.txt`).
* `fileBased`: Indicate whether this editor is file based or directory based. When file based, the editor will be asked 
to open the file representing a submission (like a `.sln` file). Otherwise, the directory will be used.
* `binary`: Path to editor binary, if any. When provided, Miu will use this path to call the editor. Otherwise, it will
ask the system to open the submission using the default program. This doesn't have to be a full path. If program is in
`$PATH`, just use the executable name.

## Building from sources

Clone this repository. Inside, run this command :

```shell
cargo run --release
```

## Changelog

See the [CHANGELOG.md](CHANGELOG.md) file for version details.

## License

This project is licensed under the GNU GPLv3. See the [LICENSE.md](LICENSE.md) file for details.

[Rust]: https://www.rust-lang.org/